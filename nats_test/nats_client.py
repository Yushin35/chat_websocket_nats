import asyncio

from nats.aio.client import Client as NATS
from aioconsole import ainput


async def recieve_message(msg):
  print('>%s' % (msg.data.decode('utf-8'),))


async def client():
    nc = NATS()

    await nc.connect(
        servers=['nats://127.0.0.1:4222'],
        connect_timeout=2,
    )
    channel = await ainput('Channel name# ')

    await nc.subscribe(channel, cb=recieve_message)
    while True:
        try:
            message = await ainput('>')
            bytes_message = bytes(message.encode('utf-8'))

            await nc.publish(channel, bytes_message)
            await nc.flush()
        except KeyboardInterrupt:
            await nc.close()
            return


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    loop.run_until_complete(client())
    loop.close()
