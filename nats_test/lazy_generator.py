def lazy_range(up_to):
    """Generator to return the sequence of integers from 0 to up_to, exclusive."""
    index = 0
    def gratuitous_refactor():
        nonlocal index
        while index < up_to:
            yield index
            index += 1
    yield from gratuitous_refactor()


def bottom():
    # Returning the yield lets the value that goes up the call stack to come right back
    # down.
    return (yield 1)

def middle():

    return (yield from bottom())

def top():

    return (yield from middle())

# Get the generator.
gen = top()
value = next(gen)
print(value)  # Prints '42'.
try:
    value = gen.send(value * 2)
except StopIteration as exc:
    value = exc.value
print(value)  # Prints '84

import asyncio
"""
# Borrowed from http://curio.readthedocs.org/en/latest/tutorial.html.
@asyncio.coroutine
def countdown(number, n):
    while n > 0:
        print('T-minus', n, '({})'.format(number))
        yield from asyncio.sleep(1)
        n -= 1

loop = asyncio.get_event_loop()
tasks = [
    asyncio.ensure_future(countdown("A", 2)),
    asyncio.ensure_future(countdown("B", 3))]
loop.run_until_complete(asyncio.wait(tasks))
loop.close()
"""
from datetime import datetime
async def compute_mul(x, y):
    print("Compute %s * %s ..." % (x, y))
    await asyncio.sleep(3.0)
    print('wake up', (x, y))
    print(datetime.now())
    return x * y


async def compute(x, y):
    print("Compute %s + %s ..." % (x, y))
    await asyncio.sleep(1.0)
    print('wake up', (x, y))
    print(datetime.now())
    return x + y

async def mul(x, y):
    print("Compute %s * %s ..." % (x, y))
    await asyncio.sleep(5.0)
    print('wake up', (x, y))
    print(datetime.now())
    return x * y

async def div(x, y):
    print("Compute %s / %s ..." % (x, y))
    await asyncio.sleep(4.0)
    print('wake up', (x, y))
    print(datetime.now())
    return x * y

async def print_sum(x, y):
    result = await compute(x, y)
    print("%s + %s = %s" % (x, y, result))

async def print_mul(x, y):
    result = await mul(x, y)
    print("%s * %s = %s" % (x, y, result))

async def print_div(x, y):
    result = await div(x, y)
    print("%s / %s = %s" % (x, y, result))

tasks = [
    asyncio.ensure_future(print_sum(1, 2)),
    asyncio.ensure_future(print_mul(3, 2)),
    asyncio.ensure_future(print_div(3, 2)),
]
loop = asyncio.get_event_loop()
loop.run_until_complete(asyncio.wait(tasks))
loop.close()