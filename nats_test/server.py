import asyncio
from uuid import uuid4

writers_dict = {

}


async def handle(reader, writer):
    my_uu_id = uuid4()
    writers_dict[my_uu_id] = writer
    while True:
        data = await reader.read(100)

        if not data:
            del writers_dict[my_uu_id]
            return

        message = data.decode()
        addr = writer.get_extra_info('peername')

        print("Received %r from %r" % (message, addr))
        print("Send: %r" % message)
        for uu_id, writer in writers_dict.items():
            if uu_id != my_uu_id:
                writer.write(data)
                await writer.drain()

loop = asyncio.get_event_loop()
coro = asyncio.start_server(handle, '127.0.0.1', 8888, loop=loop)
server = loop.run_until_complete(coro)


print('Serving on {}'.format(server.sockets[0].getsockname()))
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass


server.close()
loop.run_until_complete(server.wait_closed())
loop.close()