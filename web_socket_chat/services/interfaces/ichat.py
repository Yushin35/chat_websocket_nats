from abc import ABCMeta, abstractmethod


class IBaseChatService(metaclass=ABCMeta):
    def __init__(self, dsn):
        self.dsn = dsn

    @abstractmethod
    def save_subject_message(self, text, channel_id):
        """"""

    @abstractmethod
    def get_subject_messages(self, channel_id, count):
        """"""

    @abstractmethod
    def get_subject_by_name(self):
        """"""

    @abstractmethod
    def create_subject(self):
        """"""

    @abstractmethod
    def get_or_create_subject(self):
        """"""
