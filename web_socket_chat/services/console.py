class Info:
    @classmethod
    def head(cls):
        print('Connected to NATS servers:')

    @classmethod
    def border(cls, num=30):
        print('-' * num)

    @classmethod
    def nats_server_connect(cls, nats_servers):
        cls.head()
        cls.border()
        for server in nats_servers:
            print(server)
        cls.border()

