import asyncio

from sqlalchemy.sql import select
from aiopg.sa import create_engine


from services.interfaces.ichat import IBaseChatService
from config.settings import DSN
from services.pg.tables import (
    user_table,
    subscribe_table,
    subject_table,
    message_table,
)


class ChatService(IBaseChatService):
    def __init__(self, dsn):
        self.dsn = dsn

    async def save_subject_message(self, text, subject_id):
        async with create_engine(self.dsn) as engine:
            async with engine.acquire() as conn:
                await conn.execute(message_table.insert().values(subject_id=subject_id, text=text))

    async def get_subject_messages(self, subject_id, count=10):
        async with create_engine(self.dsn) as engine:
            async with engine.acquire() as conn:
                obj = await conn.execute(select([message_table]).where(message_table.c.subject_id == subject_id))
                result = await obj.fetchall()

                return result[:count]

    async def get_subject_by_name(self, name):
        async with create_engine(self.dsn) as engine:
            async with engine.acquire() as conn:
                obj = await conn.execute(select([subject_table]).where(subject_table.c.name == name))
                result = await obj.fetchone()
                return result

    async def create_subject(self, name):
        async with create_engine(self.dsn) as engine:
            async with engine.acquire() as conn:
                await conn.execute(subject_table.insert().values(name=name))

    async def get_or_create_subject(self, name):
        subject = await self.get_subject_by_name(name)
        if not subject:
            await self.create_subject(name)
        subject = await self.get_subject_by_name(name)

        return subject


if __name__ == '__main__':
    ch = ChatService(DSN)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(ch.get_subject_by_name('chat'))
