import asyncio

import websockets
from websockets.exceptions import ConnectionClosed
from nats.aio.client import Client as NATS

from config.settings import DSN
from config.settings import NATS_SERVERS
from services.pg.schat import ChatService
from services.console import Info


class Client:
    def __init__(self, websocket, chat_service, subject):
        self.websocket = websocket
        self.chat_service = chat_service
        self.subject = subject

    async def cd(self, msg):
        await self.chat_service.save_subject_message(msg.data.decode('utf-8'), self.subject[0])
        await self.websocket.send(msg.data.decode('utf-8'))

    async def send_unread_message(self, count=10):
        messages = await self.chat_service.get_subject_messages(self.subject[0], count)
        for message in messages:
            await self.websocket.send(message[1])


async def handle(websocket, path):
    nc = NATS()
    chat_service = ChatService(DSN)

    await nc.connect(
        servers=NATS_SERVERS,
        connect_timeout=2,
    )
    if nc.is_connected:
        Info.nats_server_connect(NATS_SERVERS)

    subject = await chat_service.get_or_create_subject(path)

    client = Client(websocket, chat_service, subject)
    sid = await nc.subscribe(path, cb=client.cd)

    await client.send_unread_message(5)

    while True:
        try:
            message = await client.websocket.recv()

            await nc.publish(path, bytes(message.encode('utf-8')))
            await nc.flush()
        except ConnectionClosed:
            await nc.unsubscribe(sid)
            return

if __name__ == '__main__':

    loop = asyncio.get_event_loop()
    websock_server = websockets.serve(handle, '127.0.0.1', 8888, loop=loop)
    server = loop.run_until_complete(websock_server)

    print('Serving on {}'.format(server.sockets[0].getsockname()))

    loop.run_forever()

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()
